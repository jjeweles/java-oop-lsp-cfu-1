package com.galvanize;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

public class Flight {
    private String departingAirport;
    private String arrivingAirport;
    private int capacity;
    protected ArrayList<Ticket> tickets = new ArrayList<>();
    private int weightLimit = 1000;

    public Flight(String departingAirport, String arrivingAirport, int capacity, int weightLimit) {
        this.departingAirport = departingAirport;
        this.arrivingAirport = arrivingAirport;
        this.capacity = capacity;
        this.weightLimit = weightLimit;
    }

    public String getDepartingAirport() {
        return departingAirport;
    }

    public String getArrivingAirport() {
        return arrivingAirport;
    }

    public List<Ticket> getTickets() {
        return tickets;
    }

    protected int totalWeight() {
        return tickets.stream().mapToInt(ticket -> ticket.getPassenger().getWeight()).sum();
    }
    public void addTicket(Ticket ticket) {
        if (totalWeight() + ticket.getPassenger().getWeight() <= weightLimit && tickets.size() < capacity) {
            tickets.add(ticket);
        }
    }

    public int getCapacity() {
        return capacity;
    }

    public BigDecimal getRevenue() {
        return tickets.stream().map(Ticket::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

}
